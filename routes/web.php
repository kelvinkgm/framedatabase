<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    // return view('welcome');
    return to_route('frames.index');
});

Route::resources([
    'frames' => App\Http\Controllers\FrameController::class,
    'patients' => App\Http\Controllers\PatientController::class,
    'suppliers' => App\Http\Controllers\SupplierController::class,
    'doctors' => App\Http\Controllers\DoctorController::class
]);

Route::resource('/patients/{patient}/eyetest', App\Http\Controllers\EyeTestController::class);

// // /// Form Bypasses
// Frame Images
Route::get('frames/{frame}/edit/image-delete',[App\Http\Controllers\FrameImageController::class, 'destroy'])->name('frame-images.destroy');
Route::delete('frames/{frame}/edit/image-delete',[App\Http\Controllers\FrameImageController::class, 'destroy'])->name('frame-images.destroy');
// Eye Test Frames
Route::get('frames/{frame}/eyetest/{eyetest}', [App\Http\Controllers\FrameController::class, 'frameSelected'])->name('frames.selected');
Route::post('frames/{frame}/eyetest/{eyetest}', [App\Http\Controllers\FrameController::class, 'frameSelected'])->name('frames.selected');