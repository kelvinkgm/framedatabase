<x-mail::message>
# Your new Glasses - What next...

Dear {{is_null($eyeTest->patient->middleName) ? $eyeTest->patient->firstLastName() : $eyeTest->patient->fullName()}},

Now that you have collected your new frame glasses with the prescribed lens, it would be best to confirm within the first 72 hours if there are any issues with the following frames:

<x-mail::table>
|     | Brand   | Frame Name       |
| --- | -------------  |:-------------:|
@foreach($eyeTest->selectedFrame as $frame)
@if(Storage::directoryExists('public/images/1') && $frame->id <= 5)
| <img src="{{Storage::url('public/images/1/'.$frame->images[0]->imageName)}}" class="img-thumbnail" width="150px" height="150px"> | {{$frame->brand}} | {{$frame->name}} |
@else
| <img src="{{Storage::url('public/images/'.$frame->brand.'-'.$frame->name.'/'.$image->imageName)}}" class="img-thumbnail" width="150px" height="150px"> | {{$frame->brand}} | {{$frame->name}} |
@endif
@endforeach
</x-mail::table>

If there are any issues after the 24 hour collection, please contact us as soon as possible, by: 
1) Emailing us by replying back to this email, or 
2) Calling us with reference to your examination date.
<x-mail::button :url="''">
Call us Now
</x-mail::button>

If there are no issues, then it is highly recommended to start using them <strong>the folling day</strong> to properly adjust from the old prescriptions.

<x-mail::panel>
### Few Notes of Taking Care of your new glasses

1) If they are not in use, ensure it is kept in a dust-free environment, such as a glass case
2) Wipe and clean the lens only with a damp towel, a glasses cleaner, or a microfiber cloth
3) Ensure that the frame is also clean by wiping around where possible debris might be around
</x-mail::panel>

We advise that after @if(\Carbon\Carbon::parse($eyeTest->patient->dob)->age <= 15) <strong>six (6) months</strong> @else <strong>two (2) years</strong> @endif from the examination date, you should visit us again for another examination, unless specified by {{$eyeTest->doctor->prefix." ".$eyeTest->doctor->firstName." ".$eyeTest->doctor->lastName}}.

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
