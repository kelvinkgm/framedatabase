<x-mail::message>
# Frames are now sent for Lens Fitting

Dear {{is_null($eyeTest->patient->middleName) ? $eyeTest->patient->firstLastName() : $eyeTest->patient->fullName()}},

This email serves as your notification email that the selected frames for you recent examiantion on {{$eyeTest->created_at->toFormattedDateString()}}, have been sent for their lens fitting.

<x-mail::table>
|     | Brand   | Frame Name       |
| --- | -------------  |:-------------:|
@foreach($eyeTest->selectedFrame as $frame)
@if(Storage::directoryExists('public/images/1') && $frame->id <= 5)
| <img src="{{Storage::url('public/images/1/'.$frame->images[0]->imageName)}}" class="img-thumbnail" width="150px" height="150px"> | {{$frame->brand}} | {{$frame->name}} |
@else
| <img src="{{Storage::url('public/images/'.$frame->brand.'-'.$frame->name.'/'.$image->imageName)}}" class="img-thumbnail" width="150px" height="150px"> | {{$frame->brand}} | {{$frame->name}} |
@endif
@endforeach
</x-mail::table>

We will notify you when your frames are ready to be collected.

If any queries, please reply back to this email or to <a href="mailto:{{$eyeTest->doctor->email}}">{{$eyeTest->doctor->prefix." ".$eyeTest->doctor->firstName." ".$eyeTest->doctor->lastName}}</a>.

Kind Regards,<br>
{{ config('app.name') }}
</x-mail::message>
