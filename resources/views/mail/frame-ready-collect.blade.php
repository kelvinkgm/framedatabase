<x-mail::message>
# Frames are Ready for Collection

Dear {{is_null($eyeTest->patient->middleName) ? $eyeTest->patient->firstLastName() : $eyeTest->patient->fullName()}},

This email serves as your formal notification that the selected frames for you recent examiantion on {{$eyeTest->created_at->toFormattedDateString()}}, have been properly fitted with your prescribed lens, and are new ready for collection for final checks.

We will be ready for you at your earliest convience, or reply to us for other queries and/or collection rescheduling.

<small><em>Please Note: We do advise the earlier you collect your new glass frames, the better for your eyes.</em></small>

Kind Regards,<br>
{{ config('app.name') }}
</x-mail::message>
