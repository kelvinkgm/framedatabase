<x-mail::message>
# Your Recent Eye Examination

Dear {{is_null($eyeTest->patient->middleName) ? $eyeTest->patient->firstLastName() : $eyeTest->patient->fullName()}},

Please find for your latest record for your eye examination on {{$eyeTest->created_at->toFormattedDateString()}} with {{$eyeTest->doctor->fullName()}}

<x-mail::table>
|    Rx    | SPH  | CYL | AXIS | ADD | PRISM |
| ----- |:------:|:-------------:|:-------------:|:------:| --------:|
| <strong>OD</strong> | {{$eyeTest->sphOD}} | {{$eyeTest->cylOD}} |{{$eyeTest->axisOD}} |{{$eyeTest->addOD}} | {{$eyeTest->prismOD}} |
| <strong>OS</strong> | {{$eyeTest->sphOS}} | {{$eyeTest->cylOS}} |{{$eyeTest->axisOS}} |{{$eyeTest->addOS}} | {{$eyeTest->prismOS}} |
</x-mail::table>

Color Perception: {{!is_null($eyeTest->colorPercept) ? $eyeTest->colorPercept : "N/A"}}

Depth Perception: {{!is_null($eyeTest->depthPercept) ? $eyeTest->depthPercept : "N/A"}}

Motility: {{!is_null($eyeTest->motility) ? $eyeTest->motility : "N/A"}}

@if(!is_null($eyeTest->notes))
<em>Doctor's Examination Notes: {{$eyeTest->notes}}</em>
@endif


If there are any queries, you can reply to the optican doctor <a href="mailto:{{$eyeTest->doctor->email}}">here</a>.
Any other further queries, please respond back to this email. <br><br>

<hr>
Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
