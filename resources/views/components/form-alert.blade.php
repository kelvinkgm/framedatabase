<div class="{{ $alertclass }}" role="alert">
    <ul style="margin:0;">
        @foreach ($errors as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    <!-- <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> -->
</div>