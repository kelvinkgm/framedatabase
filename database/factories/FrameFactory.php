<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Frame>
 */
class FrameFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $lost = fake()->boolean();
        $amount = $lost == true ? fake()->randomDigitNotNull() : null;
        return [
            'name' => fake()->word(),
            'brand' => fake()->company(),
            'color' => fake()->colorName(),
            'sku' => fake()->uuid(),
            'type' => fake()->randomElement(['Frameless', 'Half-rimmed', 'Full-rimmed', 'Sunglasses', 'Others']),
            'price' => fake()->randomFloat(2, 50, 1000),
            'qty' => fake()->randomDigitNotNull(),
            'eyeSize'=> fake()->numberBetween(40,62),
            'bridgeSize'=> fake()->numberBetween(14,24),
            'templeSize'=> fake()->numberBetween(120,150),
            'barcode' => fake()->isbn13(),
            'hasLost' => $lost,
            'lostQty' => $amount
        ];
    }
}
