<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Doctor>
 */
class DoctorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'prefix' => fake()->randomElement(['Dr.', 'Dra.']),
            'firstName' => fake()->firstName(),
            'lastName' => fake()->lastName(),
            'medLicence' => fake('en_US')->ein(),
            'contactNum' => fake('en_PH')->mobileNumber(),
            'email' => fake()->safeEmail(),
            'availableDays' => json_encode(fake()->randomElements(['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'],3)),
        ];
    }
}
