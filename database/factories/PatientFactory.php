<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Patient>
 */
class PatientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $gender = fake()->randomElement(['Male', 'Female']);
        $type = fake()->randomElement(['Walk-In', 'Company']);
        $companyName = $type == 'Company' ? fake()->company() : null;
        return [
            'firstName' => fake()->firstName($gender),
            'middleName' => fake()->lastName(),
            'lastName' => fake('en_PH')->lastName(),
            'sex' => fake()->randomElement(['M', 'F', 'Other']),
            'dob' => fake()->date(),
            'type' => $type,
            'companyName' => $companyName,
            'phone' => fake('en_PH')->mobileNumber(),
            'tel' => fake('en_PH')->mobileNumber(),
            'email' => fake()->safeEmail(),
            'address1' => fake()->streetAddress(),
            'address2' => fake()->secondaryAddress(),
            'street' => fake()->streetName(),
            'city' => fake('en_PH')->municipality(),
            'county' => fake('en_PH')->province(),
            'zipPost' => fake()->postcode(),
            'GDPRconfirmed' => fake()->boolean()
        ];
    }
}
