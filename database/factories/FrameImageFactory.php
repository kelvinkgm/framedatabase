<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\FrameImage>
 */
class FrameImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $int = 1;
        if(!Storage::directoryExists('public/images/'.$int)){
            Storage::makeDirectory('public/images/'.$int);
        }
        $image = fake()->image(public_path('storage/images/'.$int), 360, 360, 'animal', true, true, 'cats', true, 'jpg');
        // var_dump(explode(array_key_exists(count_chars($image,1)[47],$image), $image, 8));
        return [
            'imageName' => explode('/', $image, 10)[9], // Hard Coded FIX ASAP!
        ];
    }
}
