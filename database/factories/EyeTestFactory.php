<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\EyeTest>
 */
class EyeTestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'sphOD' => fake()->randomElement(['+', '-']).fake()->randomFloat(2, 1, 10),
            'cylOD' => fake()->randomElement(['+', '-']).fake()->randomFloat(2, 1, 10),
            'axisOD' => fake()->numberBetween(0,180),
            'addOD' => fake()->randomFloat(2, 1, 100),
            'prismOD' => '0.5 '.fake()->randomElement(['BU', 'BD', 'BI', 'BO']),
            'sphOS' => fake()->randomElement(['+', '-']).fake()->randomFloat(2, 1, 10),
            'cylOS'=> fake()->randomElement(['+', '-']).fake()->randomFloat(2, 1, 10),
            'axisOS' => fake()->numberBetween(0,180),
            'addOS' => fake()->randomFloat(2, 1, 100),
            'prismOS' => '0.5 '.fake()->randomElement(['BU', 'BD', 'BI', 'BO']),
            'colorPercept' => null,
            'depthPercept' => null,
            'motility' => "N/A",
            'notes' => fake()->boolean() ? fake()->text(100) : null,       
        ];
    }
}
