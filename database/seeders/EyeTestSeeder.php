<?php

namespace Database\Seeders;

use App\Models\EyeTest;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EyeTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        EyeTest::factory()->count(30)->create();
    }
}
