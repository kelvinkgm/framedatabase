<?php

namespace Database\Seeders;

use App\Models\FrameImage;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FrameImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        FrameImage::factory()->count(10)->create();
    }
}
