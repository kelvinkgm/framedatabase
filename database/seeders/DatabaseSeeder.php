<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        /// \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Admin User',
            'email' => 'admin@example.com',
            'password' => Hash::make('admin'),
        ]);
        \App\Models\User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);
        
        // Populate Database Fully!
        \App\Models\Frame::factory()->count(5)->create()->each(function($frame){
            // Create the Relationship for Frame Images
            $frame->images()->saveMany(\App\Models\FrameImage::factory()->count(3)->make());
            $frame->supplier()->saveMany(\App\Models\Supplier::factory(rand(1,3))->make());
        });
        
        $doctors = \App\Models\Doctor::factory()->count(2)->create();

        //Create Patients, Their EyeTests and associated Doctor of it
        \App\Models\Patient::factory()->count(5)->create()->each(function($patient) use ($doctors){
            $patient->tests()->saveMany(\App\Models\EyeTest::factory(rand(1,2))->make()
                ->each(function($eyeTest) use ($doctors){                    
                    $eyeTest->doctor()->associate($doctors->random());
                    // Error Recursive EyeTest unable to be utilized here
                })
            );
        });
        
        // Error Recursive EyeTest incorrectly Parenting and Childing
        foreach (\App\Models\EyeTest::all() as $test) {
            // if($test->id > $i && $test->patient->id == \App\Models\EyeTest::find($i+1)->patient->id){
                // $test->update([
                //     'testID' => \App\Models\EyeTest::all()->random()->id
                // ]);
            // }

            $test->update([
                'testID' => \App\Models\EyeTest::all()->random()->id
            ]);
        }
    }
}
