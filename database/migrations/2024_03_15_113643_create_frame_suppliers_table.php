<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('frame_suppliers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('supplierID')->comment('Associated Supplier ID');
            // $table->foreignId('supplierID')->constrained()->comment('Associated Supplier ID');
            $table->unsignedBigInteger('frameID')->comment('Associated Frame ID');
            // $table->foreignId('frameID')->constrained()->comment('Associated Frame ID');
            $table->timestamps();

            $table->foreign('frameID')->references('id')->on('frames');
            $table->foreign('supplierID')->references('id')->on('suppliers');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('frame_suppliers');
    }
};
