<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eye_test_frame', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('testID')->comment('Previous Eye Test Record ID');
            // $table->foreignId('testID')->constrained()->comment('Previous Eye Test Record ID');
            $table->unsignedBigInteger('frameID')->comment('Associated Frame ID');
            // $table->foreignId('frameID')->constrained()->comment('Associated Frame ID');
            // Lab Related Information
            $table->timestamp('labSent')->nullable()->comment('Date Sent to the Labs');
            $table->timestamp('labPickUp')->nullable()->comment("Date Notified coming back to Store");
            $table->timestamp('collectReady')->nullable()->comment("Ready for Pickup");
            $table->boolean('collected')->default(0)->comment("Date of collection");
            $table->timestamps();

            $table->foreign('frameID')->references('id')->on('frames');
            $table->foreign('testID')->references('id')->on('eye_tests');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eye_test_frame');
    }
};
