<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('eye_tests', function (Blueprint $table) {
            $table->id();
            // Foreign Keys
            $table->unsignedBigInteger('doctorID')->comment('Foreign Key to DOCTOR for the test');
            // $table->foreignId('doctorID')->constrained()->comment('Foreign Key to DOCTOR for the test');
            $table->unsignedBigInteger('patientID')->comment('Foreign Key for Patient');
            // $table->foreignId('patientID')->constrained()->comment('Foreign Key for Patient');
            $table->unsignedBigInteger('testID')->nullable()->comment('Previous Eye Test Record ID');
            // $table->foreignId('testID')->constrained()->nullable()->comment('Previous Eye Test Record ID');

            // RIGHT SIDE (OD/RE)
                // Sphere: correction for nearsightedness (-) or farsightedness (+)
            $table->string('sphOD')->comment('Right Eye Sphere [+/-]');
                // Cylinder: amount of lens power needed for astigmatism. It always follows the sphere power on an eyeglass prescription
            $table->string('cylOD')->comment('Right Eye Cylinder');
                // Axis: Lens meridian that contains no cylinder power to correct astigmatism.
            $table->string('axisOD')->comment('Right Eye Axis');
                // Add: Magnifying power applied to the bottom part of multifocal lenses to correct natural farsightedness.
            $table->string('addOD')->comment('Right Eye Additional');
                // Prism: Amount of prismatic power, measured in prism diopters ("p.d." or a triangle when written freehand), prescribed to compensate for eye alignment problems.
                // BU = base up; BD = base down; BI = base in (toward the wearer's nose); BO = base out (toward the wearer's ear)
            $table->string('prismOD')->comment('Right Eye Prism');
            
            // LEFT SIDE (OS/LE)
                // Sphere: correction for nearsightedness (-) or farsightedness (+)
            $table->string('sphOS')->comment('Left Eye Sphere [+/-]');
                // Cylinder: amount of lens power needed for astigmatism. It always follows the sphere power on an eyeglass prescription
            $table->string('cylOS')->comment('Left Eye Cylinder');
                // Axis: Lens meridian that contains no cylinder power to correct astigmatism.
            $table->string('axisOS')->comment('Left Eye Axis');
                // Add: Magnifying power applied to the bottom part of multifocal lenses to correct natural farsightedness.
            $table->string('addOS')->comment('Left Eye Additional');
                // Prism: Amount of prismatic power, measured in prism diopters ("p.d." or a triangle when written freehand), prescribed to compensate for eye alignment problems.
                // BU = base up; BD = base down; BI = base in (toward the wearer's nose); BO = base out (toward the wearer's ear)
            $table->string('prismOS')->comment('Left Eye Prism');
            
            //Others
            $table->string('colorPercept')->nullable()->comment("Color Perception");
            $table->string('depthPercept')->nullable()->comment("Depth Perception");
            // $table->string('binocularV');
            $table->string('motility')->comment("Eye Movement Control");
            // $table->string('moscopy');
            $table->longText('notes')->nullable()->comment("Eye Test Notes");
            $table->timestamps();

            $table->foreign('doctorID')->references('id')->on('doctors');
            $table->foreign('patientID')->references('id')->on('patients');
            $table->foreign('testID')->references('id')->on('eye_tests'); //Recursion Check
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('eye_tests');
    }
};
