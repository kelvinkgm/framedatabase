<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            // Name
            $table->string('firstName')->comment('First Name');
            $table->string('middleName')->nullable()->comment('Middle Name');
            $table->string('lastName')->comment('Last Name / Surname');
            $table->enum('sex', ['M', 'F', 'Other']);
            $table->date('dob')->comment('Date of Birth');
            // Type
            $table->enum('type', ['Walk-In', 'Company'])->comment('Patient Type');
            $table->string('companyName')->nullable()->comment('Company Name'); // Only if company is selected
            // Contact Details
            $table->string('phone')->nullable()->comment('Phone Number');
            $table->string('tel')->nullable()->comment('Telephone Number');
            $table->string('email')->nullable()->comment('Email Address');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('street');
            $table->string('city');
            $table->string('county');
            $table->string('zipPost')->comment('Zipcode / Postcode');
            // $table->string('country')->comment('Country')->default('Philippines');
            // Data Protection Act
            $table->boolean('GDPRconfirmed')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('patients');
    }
};
