<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('frames', function (Blueprint $table) {
            $table->id();
            // Frame
            $table->string('name')->comment('Frame Name');
            $table->string('brand')->comment('Frame Brand');
            $table->string('sku')->comment('Frame SKU Number')->unique();
            // $table->string('model')->comment('Frame Model');
            $table->string('color')->comment('Frame Color');
            $table->enum('type', ['Frameless', 'Half-rimmed', 'Full-rimmed', 'Sunglasses', 'Others'])->comment('Frame Type');
            // Cost and Amount
            $table->float('price', 8, 2)->comment('Frame Price');
            $table->integer('qty')->default(0)->comment('Frame Quantity');
            // Frame Details
            $table->integer('eyeSize')->comment('Frame Eye Size');
            $table->integer('bridgeSize')->comment('Frame Nose Bridge Size');
            $table->integer('templeSize')->comment('Frame Arm/Temple Size');
            $table->string('barcode')->nullable()->comment('Frame Barcode ID')->unique();
            // Lost Frames
            $table->boolean('hasLost')->comment('Boolean: Missing frames')->default(0);
            $table->integer('lostQty')->nullable()->comment('Missing Frame Quantity')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('frames');
    }
};
