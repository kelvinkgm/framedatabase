<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('frame_images', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('frameID')->comment('Associated Frame Image');
            // $table->foreignId('frameID')->constrained()->comment('Associated Frame Image');
            $table->string('imageName')->comment('Image File Name');
            $table->timestamps();

            $table->foreign('frameID')->references('id')->on('frames');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('frame_images');
    }
};
