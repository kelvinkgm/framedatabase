<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->enum('prefix', ['Dr.', 'Dra.'])->comment('Doctor Prefix');
            $table->string('firstName')->comment('Doctor First Name');
            $table->string('lastName')->comment('Doctor Last Name');
            $table->string('medLicence')->comment('Medical Licence');
            $table->string('contactNum')->comment('Contact Number');
            $table->string('email')->unique()->comment('Contact Email Address');            
            $table->json('availableDays')->comment('Available Days');
            $table->timestamps();
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('doctors');
    }
};
