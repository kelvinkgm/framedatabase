<?php

namespace App\Services;

use App\Models\Frame;
use App\Models\FrameImage;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class FrameImageService
{
    public function store(Frame $frame, $images)
    {
        // Create Frame Image Directory
        Storage::makeDirectory('public/images/' . $frame->brand . '-' . $frame->name);

        // Create internal counter
        $int = 1;

        // Retrieve the files from the request
        foreach ($images as $image) {
            // Set location path
            $location = public_path('storage/images/' . $frame->brand . '-' . $frame->name . '/');

            // Set new file names
            $imageName = $frame->brand . '-' . $frame->name . '-' . $int . '.' . $image->getClientOriginalExtension();

            // Get Image from Request
            $img = Image::make($image);

            // Resize under aspect ratio
            $img->resize(null, 450, function ($constraint) {
                $constraint->aspectRatio();
            });

            // Save the Image to Storage
            $img->save($location . $imageName);

            // Create a new File model and associate it with the user
            $frame->images()->create([
                'imageName' => $imageName,
            ]);

            // Increase by 1
            $int++;
        }
    }

    public function formDestroy(FrameImage $frameImage){

    }

    public function destroy(FrameImage $frameImage){
        // Remove Image in Storage Directory
        if(Storage::directoryExists('public/images/1') && $frameImage->frameID <= 5){
            Storage::delete('public/images/1/'.$frameImage->imageName);
            if(Storage::assertDirectoryEmpty('public/images/1/')){
                Storage::deleteDirectory('public/images/1/');
            }
        } else {
            Storage::delete('public/images/'.$frameImage->frame->brand.'-'.$frameImage->frame->name.'/'.$frameImage->imageName);
            if(Storage::assertDirectoryEmpty('public/images/'.$frameImage->frame->brand.'-'.$frameImage->frame->name.'/')){
                Storage::deleteDirectory('public/images/'.$frameImage->frame->brand.'-'.$frameImage->frame->name.'/');
            }
        }
        // Delete Core Record
        $frameImage->delete($frameImage->id);
    }
}
