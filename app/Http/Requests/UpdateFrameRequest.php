<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFrameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'brand'  => 'required|string',
            'color' => 'required|string',
            'sku' => 'required|string',
            'type' => 'required',
            'price' => 'required|decimal:2',
            'qty' => 'required|numeric|min:0',
            'eyeSize' => 'required|numeric',
            'bridgeSize' => 'required|numeric',
            'templeSize' => 'required|numeric',
            'hasLost' =>'sometimes|required|boolean',
            'lostQty' => 'exclude_unless:hasLost,true|required|numeric',
        ];
    }
}
