<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'firstName' => 'required|string',
            'middleName' => 'sometimes|string',
            'lastName' => 'required|string',
            'sex' => 'required',
            'dob' => 'required|date',
            'type' => 'required',
            'companyName' => 'exclude_unless:type,Company|string',
            'phone' => 'required|string',
            'tel' => 'required|string',
            'email' => 'required|email',
            'address1' => 'required|string',
            'address2' => 'nullable|string',
            'street' => 'required|string',
            'city' => 'required|string',
            'county' => 'required|string',
            'zipPost' => 'required|string',
            'GDPRconfirmed' => 'required|accepted',
        ];
    }
}
