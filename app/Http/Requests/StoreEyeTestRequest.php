<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEyeTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'sphOD' => 'required|string',
            'cylOD' => 'required|string',
            'axisOD' => 'required|string',
            'addOD' => 'required|string',
            'prismOD' => 'required|string',
            'sphOS' => 'required|string',
            'cylOS' => 'required|string',
            'axisOS' => 'required|string',
            'addOS' => 'required|string',
            'prismOS' => 'required|string',
            'colorPercept' => 'required|string',
            'depthPercept' => 'required|string',
            'motility' => 'sometimes|nullable|string',
            'notes' => 'sometimes|nullable|string',
            'doctorID' => "integer",
            'patientID' => "integer",
            'testID' => 'integer',
        ];
    }
}
