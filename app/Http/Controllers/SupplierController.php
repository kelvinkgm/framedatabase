<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSupplierRequest;
use App\Http\Requests\UpdateSupplierRequest;
use App\Models\Supplier;
use Illuminate\Support\Facades\Auth;

class SupplierController extends Controller
{
    /**
     * Instantiate a new controller instance.
     */
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('log');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // return Supplier::all();
        if(Auth::user()){
            return view('suppliers.index', [
                'suppliers' => Supplier::simplePaginate(5)
            ]);
        }else {
            return abort(404,"Not Found");
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        if(Auth::user()){
            return view('suppliers.create');
        } else {
            return abort(404, "Not Found");
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSupplierRequest $request)
    {
        $validated = $request->validated();
        $supplier = Supplier::create($validated);
        return to_route('suppliers.index')->with([
            'status' => 'Supplier '.$supplier->name." has saved correctly",
            'type' => 'success'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Supplier $supplier)
    {
        return $supplier;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Supplier $supplier)
    {
        if(Auth::user()){
            return view('suppliers.edit', [
                'supplier' => $supplier
            ]);
        } else {
            return abort(404, "Not Found");
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSupplierRequest $request, Supplier $supplier)
    {
        if(Auth::user()){
            $validated = $request->validated();
            $supplier->update($validated);
            return to_route('suppliers.index')->with([
                'status' => $supplier->name.' has been updated',
                'type' => 'success'
            ]);

        } else {
            return abort('404');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Supplier $supplier)
    {
        //
    }

    public function forceDestroy(){
        
    }

    public function restore(Supplier $supplier)
    {
        $supplier->restore();
    }
}
