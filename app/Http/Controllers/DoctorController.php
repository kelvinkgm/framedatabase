<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDoctorRequest;
use App\Http\Requests\UpdateDoctorRequest;
use App\Models\Doctor;
use Illuminate\Support\Facades\Auth;

class DoctorController extends Controller
{
    
    /**
     * Instantiate a new controller instance.
     */
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('log');
    }


    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // return abort('401');
        if(Auth::user()){
            return view('doctors.index', [
                'doctors' => Doctor::simplePaginate(5),
            ]);
        } else {
            return abort('401');
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return abort('401');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDoctorRequest $request)
    {
        return abort('401');
    }

    /**
     * Display the specified resource.
     */
    public function show(Doctor $doctor)
    {
        return view('view.name', $doctor);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Doctor $doctor)
    {
        return abort('401');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateDoctorRequest $request, Doctor $doctor)
    {
        return abort('401');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Doctor $doctor)
    {
        if(Auth::user()){
            $doctor->delete();
        } else {
            return abort('401');
        }
    }
}
