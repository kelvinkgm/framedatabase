<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFrameRequest;
use App\Http\Requests\UpdateFrameRequest;
use App\Http\Requests\updateQuickFrameRequest;
use App\Models\Frame;
use App\Services\FrameImageService;
use Illuminate\Support\Facades\Auth;

class FrameController extends Controller
{

    private $frameImageService;

    // Middleware constraint
    public function __construct(FrameImageService $frameImageService)
    {
        // $this->middleware('auth',['except' => ['index', 'show']]);
        $this->frameImageService = $frameImageService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('frames.index', [
            'frames' => Auth::user() ? Frame::withTrashed()->simplePaginate(6) : Frame::simplePaginate(6)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('frames.create',[
            'suppliers' => \App\Models\Supplier::all(),
            'types' => ["Frameless","Half-rimmed","Full-rimmed","Sunglasses","Others"]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFrameRequest $request)
    {
        // dd($request->user()->can('create'), $this->authorize('create', Frame::class),$request);
        if(Auth::user() && $request->authorize() 
            // && $this->authorize('create', Frame::class)
        ){
            $validated = $request->validated();
            $validated['barcode'] = $request->sku; //Create Barcode 
            $frame = Frame::create($validated); // Create Frame
            $frame->supplier()->attach($request->supplierID); //Link Frame to Suppliers
            // Link Images
            $this->frameImageService->store($frame, $request->file('images')); // Service Link to Image Save

            return to_route('frames.show', $frame->id)->with([
                'status' => "Frame and associated images has been saved",
                'type' => 'success'
            ]);
        } else {
            return abort(404, "Not Found");
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Frame $frame)
    {
        return view('frames.show', [
            'frame' => Frame::withTrashed()->find($frame->id),
            'eyetests' => \App\Models\EyeTest::whereDate('created_at', \Carbon\Carbon::today())->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Frame $frame)
    {
        return view('frames.edit', [
            'frame' => Frame::withTrashed()->find($frame->id),
            'suppliers' => \App\Models\Supplier::all(),
            'types' => Frame::types(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFrameRequest $request, Frame $frame)
    {
        if ($request->user()->cannot('update', $frame)) {
            abort(403);
        }
        
        if(Auth::user() && $request->authorize()){
            $validated = $request->validated();
            $validated['barcode'] = $request->sku; //Update Barcode if SKU changes
            $validated = Frame::qtyCheck($validated); // Validate the Quantity Checks
            $frame->update($validated); // Update the Frame Information
            $lostCheck = Frame::alertArray($validated); //Array Alert
            $frame->supplier()->sync($request->supplierID, false); //Sync Suppliers to Frame
            return to_route('frames.show', $frame->id)->with($lostCheck);
        } else {
            return abort(404, "Not Found");
        }
    }

    public function quickUpdate(updateQuickFrameRequest $request, Frame $frame){
        if(Auth::user() && $request->authorize()){
            $validated = $request->validated();
            $validated = Frame::qtyCheck($validated); // Validate the Quantity Checks
            $frame->update($validated); // Update the Frame Information            
            if(!is_null($request->file('images'))){
                // Link Images
                $this->frameImageService->store($frame, $request->file('images')); // Service Link to Image Save
            }
            return to_route('frames.index')->with(Frame::alertArray($validated));
        } else {
            return abort('404', "Not Found");
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Frame $frame)
    {
        if(Auth::user()){
            $frame->delete();
            return to_route('frames.index')->with([
                'status' => $frame->name.' has been disabled.',
                'type' => 'info'
            ]);
        } else {
            return abort(404,'Not Found');
        }
    }

    /**
     * Force Remove the specified resource from storage
     */
    public function forceDestroy(Frame $frame)
    {
        if(Auth::user()){
            foreach ($frame->images as $frameImage) {
                $this->frameImageService->destroy($frameImage);
            }
            $frame->supplier()->detach();
            $frame->forceDelete(); 
            return to_route('frames.index')->with([
                'status' => $frame->name.' and associated images have been deleted.',
                'type' => 'success'
            ]);
        } else {
            return abort(404,'Not Found');
        }
    }

    /**
     * Restore the specified resource from storage
     */
    public function restore($frame)
    {
        if(Auth::user()){
            $frame = Frame::withTrashed()->find($frame);
            $frame->restore();
            return to_route('frames.index')->with([
                'status' => $frame->name.' has been restored.',
                'type' => 'info'
            ]);
        } else {
            return abort(404, "Not Found");
        }
    }

    /**
     *  Select the specified resource for EyeTestFrame Pivot with the associated test
     */
    public function frameSelected(Frame $frame, $eyeTestID){
        if(Auth::user()){
            $eyeTest = \App\Models\EyeTest::find($eyeTestID);
            $eyeTest->selectedFrame()->attach([$frame->id], ['labSend' => now()]); // Sync Examination to Frame
            $frame->update(['qty' => ($frame->qty - 1)]); // Quantity Change
            //Set Up Notification
            $eyeTest->patient->notify((new FrameLabSent($eyeTest)))->withDelay();
            //Redirect
            return to_route('patients.show', $eyeTest->patient)->with([
                'status' => $frame->brand.' - '.$frame->name." has now been selected for this patient's latest examination (ID: ".$eyeTest->id.")",
                'type' => "success"
            ]);
        } else {
            return abort(404, 'Not Found');
        }
    }
}
