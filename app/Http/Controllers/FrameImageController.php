<?php

namespace App\Http\Controllers;

use Intervention\Image\Image;
use App\Http\Requests\StoreFrameImageRequest;
use App\Http\Requests\UpdateFrameImageRequest;
use App\Models\FrameImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FrameImageController extends Controller
{
    /**
     * Instantiate a new controller instance.
     */
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('log');
    }
    
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return abort(404, "Not Found");
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return abort(404, "Not Found");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFrameImageRequest $request)
    {
        $frame = \App\Models\Frame::find($request->id);
        // Create Frame Image Directory
        Storage::makeDirectory('public/images/'.$frame->brand.'-'.$frame->name);
        // Create internal counter
        $int = 1;
        // Retrieve the files from the request
        foreach ($request->file('images') as $image) {
            // Set location path
            $location = public_path('storage/images/'.$frame->brand.'-'.$frame->name.'/');
            // Set new file names
            $imageName = $frame->brand.'-'.$frame->name.'-'.$int.'.'.$image->getClientOriginalExtension(); //OrgExt or JPGExt?
            //Get Image from Request
            $img = Image::make($image); 
            // Resize under aspect ratio
            $img->resize(null, 450, function ($constraint) {
                $constraint->aspectRatio();
            });
            // Save the Image to Storage
            $img->save($location.$imageName);
            // Create a new File model and associate it with the user
            $frame->images()->create([
                'imageName' => $imageName,
            ]);
            // Increase by 1
            $int++;
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFrameImageRequest $request, FrameImage $frameImage)
    {
        $frame = \App\Models\Frame::find($frameImage->id);
        // Create Frame Image Directory
        Storage::makeDirectory('public/images/'.$frame->brand.'-'.$frame->name);
        // Create internal counter based on amount
            // TODO: Conflict issue for similar numbers (eg: img-2 stored ; img-2 to save)
        $int = $frame->images()->count();
        // Retrieve the files from the request
        foreach ($request->file('images') as $image) {
            // Set location path
            $location = public_path('storage/images/'.$frame->brand.'-'.$frame->name.'/');
            // Set new file names
            $imageName = $frame->brand.'-'.$frame->name.'-'.$int.'.'.$image->getClientOriginalExtension(); //OrgExt or JPGExt?
            //Get Image from Request
            $img = Image::make($image); 
            // Resize under aspect ratio
            $img->resize(null, 450, function ($constraint) {
                $constraint->aspectRatio();
            });
            // Save the Image to Storage
            $img->save($location.$imageName);
            // Create a new File model and associate it with the user
            $frame->images()->create([
                'imageName' => $imageName,
            ]);
            // Increase by 1
            $int++;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FrameImage $frameImage)
    {
        dd('hit');
        if(Auth::user()){
            $frame = \App\Models\Frame::find($$frameImage->frameID)->first();
            // Remove Image in Storage Directory
            if(Storage::directoryExists('public/images/1') && $frameImage->frameID <= 5){
                Storage::delete('public/images/1/'.$frameImage->imageName);
            } else {
                Storage::delete('public/images/'.$frame->brand.'-'.$frame->name.'/'.$frameImage->imageName);
            }
            // Delete Record
            FrameImage::destroy($frameImage->id);
            // Return back to the edit 
            return to_route('frames.edit', $frame);
        } else {
            return abort(404, "Not Found");
        }
    }
}
