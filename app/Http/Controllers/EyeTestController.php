<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEyeTestRequest;
use App\Http\Requests\UpdateEyeTestFrameRequest;
use App\Http\Requests\UpdateEyeTestRequest;
use App\Models\EyeTest;
use Illuminate\Support\Facades\Auth;

class EyeTestController extends Controller
{
    /**
     * Instantiate a new controller instance.
     */
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('log');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return abort(401);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(\App\Models\Patient $patient)
    {
        // return abort(401);
        return view('eyetests.create',[
            'doctors' => \App\Models\Doctor::all(),
            'patient' => $patient
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEyeTestRequest $request, \App\Models\Patient $patient)
    {
        if(Auth::user() && $request->authorize()){
            $validated = $request->validated();
            // $test = EyeTest::where('patientID', $request->patientID)->latest()->first(); //Locate recent records of Acted on patient
            $test = EyeTest::whereBelongsTo($request->patientID)->latest()->first(); //Locate recent records of Acted on patient
            // Empty or known test records ? First Test Record to be null'd ; Sync Test ID with previous ID
            empty($test) ? $test = null : $validated['testID'] =  $test->id;
            EyeTest::create($validated); // Create Eye Test
            // Redirect
            return to_route('patients.show', \App\Models\Patient::findOrFail($request->patientID))->with([
                "status" => "Eye Examination is recorded to this patient.",
                "type" => "success",
            ]);
        } else {
            return abort('401');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(EyeTest $eyeTest)
    {
        return abort(401);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(EyeTest $eyeTest)
    {
        return abort(401);
    }

    public function updateStatus(UpdateEyeTestFrameRequest $request, \App\Models\Patient $patient, $eyeTest, $pivotid)
    {
        // dd($request);
        if(Auth::user() && $request->authorize()){
            $validated = $request->validated();
            // Repopulate Params
            $pivotid = json_decode($pivotid);
            $eyeTest = $patient->tests()->find($eyeTest); // EyeTest Model Object
            if(array_key_exists('labPickUp', $validated) && $validated['labPickUp'] == 'on' && !array_key_exists('collectReady', $validated)){
                foreach($pivotid as $pid){
                    $frame = $eyeTest->selectedFrame()->where('eyetest_frames.id', $pid)->first();
                    // Singular EyeTest Frame Association
                        // $eyeTestFrame = $frame->pivot;
                        // $eyeTestFrame->update(['labPickUp' => \Carbon\Carbon::now()]);
                    // Mass Update All Associated Frames of the Test
                        $frame->eyeTests()->updateExistingPivot($eyeTest->id,[
                            'labPickUp' => \Carbon\Carbon::now()
                        ]);
                }
                //Set up Notification
                $patient->notifyNow((new FrameReadyCollect($eyeTest)));
            } else if(array_key_exists('collectReady', $validated) || $validated['collectReady'] == 'on') {
                foreach($pivotid as $pid){
                    $frame = $eyeTest->selectedFrame()->where('eyetest_frames.id', $pid)->first();
                    // Singular EyeTest Frame Association
                        // $eyeTestFrame = $frame->pivot;
                        // $eyeTestFrame->update(['labPickUp' => \Carbon\Carbon::now()]);
                    // Mass Update All Associated Frames of the Test
                    $frame->eyeTests()->updateExistingPivot($eyeTest->id,[
                        'collectReady' => \Carbon\Carbon::now(),
                        'collected' => 1
                    ]);
                }
                if($patient->email){
                    //Set up & Queue mail
                    Mail::to($eyeTest->patient->email)->later(now()->addHours(1), new FrameCollected($eyeTest));
                }
            } else {
                return abort(404);
            }
            return to_route('patients.show', $patient)->with([
                'type' => 'success',
                'status' => "Frame Status for ".$frame->name." with EyeTest ID: ".$eyeTest->id." has been updated"
            ]);
        } else {
            return abort('401');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(EyeTest $eyeTest)
    {
        return abort(401);
    }
}
