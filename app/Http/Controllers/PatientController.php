<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePatientRequest;
use App\Http\Requests\UpdatePatientRequest;
use App\Models\Patient;
use Illuminate\Support\Facades\Auth;

class PatientController extends Controller
{
    /**
     * Instantiate a new controller instance.
     */
    public function __construct(){
        // $this->middleware('auth')->except(['create']);
        // $this->middleware('log')->only(['store', 'update', 'delete']);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if(Auth::user()){
            return view('patients.index')->with([
                'patients' => Patient::simplePaginate(5),
            ]);
        } else {
            return abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePatientRequest $request)
    {
        if(Auth::user()){
            $validated = $request->validated();
            $validated['GDPRconfirmed'] = 1;
            // dd($validated, $request);
            $patient = Patient::create($validated);
            return to_route('eyetest.create', [
                'patient' => $patient
            ]);            
        } else {
            return abort(404);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Patient $patient)
    {
        return view('patients.show', [
            'patient' => $patient
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Patient $patient)
    {
        if(Auth::user()){
            return view('patients.edit',[
                'patient' => $patient
            ]);
        } else {
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePatientRequest $request, Patient $patient)
    {
        if(Auth::user()){
            $validated = $request->validated();
            dd($validated, $request);
            $patient = Patient::store($validated);
        } else {
            return abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Patient $patient)
    {
        if(Auth::user()){
            //Send EyeTest & Doctor Contact information to Patient

            //Delete
            $patient->delete(); // Validate for EyeTest Relations
        } else {
            return abort(404);
        }
    }
}
