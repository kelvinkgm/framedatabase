<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Frame extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'brand',
        'color',
        'type',
        'price',
        'qty',
        'sku',
        'eyeSize',
        'bridgeSize',
        'templeSize',
        'hasLost',
        'lostQty'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'barcode',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [];

    /***********************************
     * Laravel Eloquent Model Relationships
     ***********************************/
    
    /**
     * Get all of the comments for the Frame
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(FrameImage::class, 'frameID', 'id');
    }

    /**
     * Bring Associated Frames of the supplier.
     */
    public function supplier()
    {
        return $this->belongsToMany(Supplier::class, 'frame_suppliers', 'frameID', 'supplierID')->withTimestamps();
    }

    /**
     * Get associated Eye Test
     */
    public function eyeTests()
    {
        return $this->belongsToMany(EyeTest::class, 'eyetest_frames', 'frameID', 'testID')->using(EyeTestFrame::class)->withPivot(['labSent', 'labPickUp','collectReady', 'collected'])->withTimestamps();
    }

    /***********************************
     * Specific Model Functions
     ***********************************/

    /**
     * Type of Frames avaliable
     * 
     * @return Array of Types
     */
    protected function types(): Array {
        return ["Frameless","Half-rimmed","Full-rimmed","Sunglasses","Others"];
    }

    /**
     * Quantity Checker under a Frame Lost situation
     * 
     * @return Validated Frame Information w/ checker updates
     */
    protected function qtyCheck(Array $array): Array {
        // If hasLost key exists in the array?
        if(array_key_exists('hasLost', $array)){
            // Check Calculation of Quanity over lost 
            if($array['qty'] == $array['lostQty'] || $array['qty'] <= 0){
                // Always 0 regardless
                $array['qty'] = 0;
            } else {
                // Subtract lost from total Quantity
                $array['qty'] -= $array['lostQty'];
            }
        } else {
            //Reset
            $array['lostQty'] = 0;
            $array['hasLost'] = 0;
        }
        return $array;
    }

    /**
     * (Suitable for Updates) Provide Alert type and status based on QtyCheck function
     * 
     * @return Array of Alert information
     */
    protected function alertArray(Array $array): Array
    {
        $lostCheck = null;
        if(array_key_exists('hasLost', $array) && $array['hasLost'] == true){
            $lostCheck = [
                'status' => $array['brand'].' - '.$array['name']." has updated, however there are ".$array['lostQty']." has been lost.",
                'type' => 'warning'
            ];
        } else {
            $lostCheck = [
                'status' => "Frame ".$array['brand'].' - '.$array['name']." has updated",
                'type' => 'success'
            ];
        }
        return $lostCheck;
    }

    /**
     * Get Path to frame Image
     */
    public function imagePath(Frame $frame, bool $single = true, int $idx = null) {
        if($single){
            return ($idx !== null) && $frame->id <= 5
                ? Storage::url('public/images/1/' . $frame->images[$idx]->imageName)
                : Storage::url('public/images/'.$frame->brand.'-'.$frame->name.'/'. $frame->images[$idx]->imageName);
        } else {
            return $frame->id <= 5
                ? Storage::url('public/images/1/'.$frame->images[$idx]->imageName)
                : Storage::url('public/images/'.$frame->brand.'-'.$frame->name.'/'.$frame->images[$idx]->imageName);
        }
    }
}
