<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EyeTest extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [        
        'sphOD', 'cylOD', 'axisOD', 'addOD', 'prismOD',
        'sphOS', 'cylOS', 'axisOS', 'addOS', 'prismOS',
        'colorPercept', 'depthPercept', 'motility', 'notes', 'doctorID', 'patientID', 'testID'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['notes', 'doctorID', 'patientID','testID'];

    /***********************************
     * Laravel Eloquent Model Relationships
     ***********************************/
    /**
     * Get the Doctor that did the EyeTest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function doctor()
    {
        return $this->belongsTo(Doctor::class, 'doctorID', 'id');
    }

    /**
     * Get the Previous Test that owns the EyeTest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function previousTest()
    {
        return $this->belongsTo(EyeTest::class, 'testID', 'id');
    }

    /**
     * Get the EyeTest associated with the EyeTest
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function nextTest()
    {
        return $this->hasOne(EyeTest::class, 'id', 'testID');
    }

    /**
     * Get the Patient that owns the EyeTest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patientID', 'id');
    }

    /**
     * Get the associated Frame to the Test
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function selectedFrame()
    {
        return $this->belongsToMany(Frame::class, 'eyetest_frames','testID','frameID')->using(EyeTestFrame::class)->withPivot(['labSent', 'labPickUp','collectReady', 'collected'])->withTimestamps();
    }  

    /***********************************
     * Specific Model Functions
     ***********************************/
}
