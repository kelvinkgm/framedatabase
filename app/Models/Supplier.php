<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name', 'contactName', 'email', 'number'
    ];

    protected $hidden = [
        'email', 'number'
    ];

    /***********************************
     * Laravel Eloquent Model Relationships
     ***********************************/

    /**
     * Bring Associated Frames of the supplier.
     */
    public function frames()
    {
        return $this->belongsToMany(Frame::class, 'frame_suppliers', 'supplierID', 'frameID')->withTimestamps();
    }
}
