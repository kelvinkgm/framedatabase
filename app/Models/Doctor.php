<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'prefix',
        'firstName',
        'lastName',
        'contactNum',
        'availableDays',
        'medLicine'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ['medLicine', 'avaliableDays'];

    /***********************************
     * Laravel Eloquent Model Relationships
     ***********************************/
    /**
     * Get all of the EyesTest done by the Doctor
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function examinations()
    {
        return $this->hasMany(EyeTest::class, 'doctorID', 'id');
    }

    public function isUser()
    {
        return $this->hasOne(User::class, 'id');
    }

    /***********************************
     * Specific Model Functions
     ***********************************/

    public function fullName(): string
    {
        return $this->prefix.' '.$this->firstName.' '.$this->lastName;
    }

    public function availableDays(): string
    {
        $string = '';
        foreach(json_decode($this->availableDays) as $days){
            $string = $string.$days.', ';
        }
        return $string;
    }

}
