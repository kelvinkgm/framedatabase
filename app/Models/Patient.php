<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Patient extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'firstName', 'middleName','lastName', 'sex', 'dob', 'type', 'companyName', 'city', 'county',
        'phone', 'tel', 'email', 'address1', 'address2', 'street', 'country','zipPost','GDPRconfirmed'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'phone', 'tel', 'email', 'address1', 'address2', 'street', 'country','zipPost','GDPRconfirmed'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'GDPRconfirmed' => true
    ];

    /***********************************
     * Laravel Eloquent Model Relationships
     ***********************************/
    /**
     * Get all of the EyeTests for the Patient
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tests()
    {
        return $this->hasMany(EyeTest::class, 'patientID', 'id');
    }

    /***********************************
     * Specific Model Functions
     ***********************************/

    /**
     * Get Full Name of Patient
     */
    public function fullName(): string
    {
    return $this->firstName.' '.$this->middleName.' '.$this->lastName;
    }

    /**
     * Get Short Full Name of Patient
     */
    public function firstLastName(): string
    {
    return $this->firstName.' '.$this->lastName;
    }

    /**
     * Get Address
     */
    public function fullAddress(): string
    {
    return $this->address1.' '.$this->address2.PHP_EOL.$this->street.PHP_EOL.$this->city.PHP_EOL.$this->zipPost.' '.$this->county;
    }

    /**
     * Get the Patient Type
     */
    public function patientType(): string
    {
    return $this->type === "Company" ? $this->type.' - '.$this->companyName : $this->type;
    }
}
