<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Attachment;
use Illuminate\Support\Facades\Storage;

class FrameImage extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ['imageName'];

    /***********************************
     * Laravel Eloquent Model Relationships
     ***********************************/

    /**
     * Get the associated Frame.
     */
    public function frame()
    {
        return $this->belongsTo(Frame::class, 'frameID', 'id');
    }

    /***********************************
     * Specific Model Functions
     ***********************************/

    /**
     * Get the attachable representation of the model.
     */
    public function toMailAttachment()
    {
        return Attachment::fromPath('/path/to/file');
    }

    /**
     * Get Image Path in full (Not LINKED TO ANYWHERE YET)
     */
    public function imagePath(FrameImage $frameImage, bool $single = true, int $idx = null) {
        if($single){
            return $idx !== null
                ? Storage::url('public/images/1/' . $frameImage[$idx]->imageName)
                : Storage::url('public/images/'.$frameImage->frame->brand.'-'.$frameImage->frame->name.'/'. $frameImage->imageName);
        } else {
            return $idx !== null
                ? Storage::url('public/images/1/' . $frameImage[$idx]->imageName)
                : Storage::url('public/images/'.$frameImage->frame->brand.'-'.$frameImage->frame->name.'/'. $frameImage->imageName);
        }
    }
}
