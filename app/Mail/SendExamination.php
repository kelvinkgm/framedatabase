<?php

namespace App\Mail;

use App\Models\EyeTest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class SendExamination extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(public EyeTest $eyeTest)
    {
        //
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Your Recent Eye Examination',
            from: new Address('no-reply@example.com', 'Optical Admin'),
            replyTo: [
                new Address('admin@example.com', 'Admin')
            ],
            tags:['eye_examination', 'optical'],
            metadata: [
                'eyeTestId' => $this->eyeTest->id
            ]
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            markdown: 'mail.send-examination',
            with: [
                
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [
            // Attachment::fromData(fn () => $this->eyeTest->pdf, 'Report.pdf')->withMime('application/pdf'),
        ];
    }
}
