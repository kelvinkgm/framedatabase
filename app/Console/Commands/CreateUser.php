<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'app:create-user';
    protected $signature = 'user:create
        {name : Name of new Admin User (Use \'" "\' to provide full name)}
        {email : Email address of new Admin User}
        {doc? : Optional - Is new Admin User a Doctor}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Admin Users';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $doctor = $this->argument('doc');
        $password = $this->secret('What is this new user`s password');
        //Create User
        $user = new \App\Models\User();
        $user->name = $this->argument('name');
        $user->email = $this->argument('email');
        $user->password = \Illuminate\Support\Facades\Hash::make($password);
        if($doctor){
            $user->isDoc = 1;
            $user->save();
            // 
            $this->info('User Profile Created.');
            $this->question("You stated you are a doctor.");
            $this->info("Starting Doctor linking");
            $doc = new \App\Models\Doctor();
            $doc->prefix = $this->choice(
                'Prefix to be used?',
                ['Dr.', 'Dra.'],
            );
            $names = explode(' ', $this->argument('name'));
            $doc->firstName = $names[0];
            $doc->lastName = $names[1];
            $doc->medLicence = $this->ask('Please provide your medical licince number.');
            $doc->contactNum = $this->ask('Please provice a contact number. ');
            $doc->availableDays = json_encode($this->choice(
                'What days are you considered to typically be allowing customers generally each week?',
                ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                null,
                null,
                true
            ));
            $doc->save();
            $this->info('Doctor Info Saved');
        } else {
            $user->save();
            // 
            $this->info('User Profile Created.');
        }
    }
}
